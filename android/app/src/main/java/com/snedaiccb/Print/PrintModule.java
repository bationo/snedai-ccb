package com.snedaiccb.Print;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.BaseActivityEventListener;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.IllegalViewOperationException;
import com.snedaiccb.Print.Activities.PrintActivity;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import java.util.HashMap;
import java.util.Map;
import com.facebook.react.bridge.Promise;


import javax.annotation.Nullable;

public class PrintModule extends ReactContextBaseJavaModule {

    private static final String DURATION_SHORT_KEY = "SHORT";
    private static final String DURATION_LONG_KEY = "LONG";
    private ReactApplicationContext _context;
    private static final String E_PRINT_ERROR = "E_PRINT_ERROR";
    private static final int IMAGE_PRINT_REQUEST = 65530;
    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    private static final String E_PRINT_CANCELLED = "E_PRINT_CANCELLED";
    private static final String E_FAILED_TO_SHOW_PICKER = "E_FAILED_TO_SHOW_PICKER";
    private static final String E_NO_IMAGE_DATA_FOUND = "E_NO_IMAGE_DATA_FOUND";

    private Promise mPickerPromise;

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {

        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
            if (requestCode == IMAGE_PRINT_REQUEST) {
                WritableMap map = Arguments.createMap();
                    map.putString("status", intent.getStringExtra("status"));
                map.putString("signature", intent.getStringExtra("signature"));
                map.putString("url", intent.getStringExtra("url"));
                mPickerPromise.resolve(map);

                /*else{
                    map.putString("status", "0");
                    map.putString("signature", "");
                    map.putString("url", "");
                }*/



            }
        }
    };


    public PrintModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this._context = reactContext;
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    @Override
    public String getName() {
        return "Print";
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();
        constants.put(DURATION_SHORT_KEY, Toast.LENGTH_SHORT);
        constants.put(DURATION_LONG_KEY, Toast.LENGTH_LONG);
        return constants;
    }

    private void sendEvent(ReactContext reactContext,
                           String eventName,
                           @Nullable WritableMap params) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                .emit(eventName, params);
    }

    @ReactMethod
    public void show(String message, int duration) {
        newEnrol();
    }

    @ReactMethod
    void navigateToExample(final Promise promise) {

        Activity currentActivity = getCurrentActivity();

        if (currentActivity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }

        // Store the promise to resolve/reject when picker returns data
        mPickerPromise = promise;

        try {
            final Intent intent = new Intent(currentActivity, PrintActivity.class);
            currentActivity.startActivityForResult(intent, IMAGE_PRINT_REQUEST);
        } catch (Exception e) {
            // mPickerPromise.reject(E_FAILED_TO_SHOW_PICKER, e);
          //  mPickerPromise = null;
        }
    }



    private void newEnrol(){

       /* try {
            WritableMap map = Arguments.createMap();
            map.putString("emprinte", "bationo");
            promise.resolve(map);

          /*  ReactApplicationContext context = getReactApplicationContext();
            Intent intent = new Intent(context, PrintActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        } catch (IllegalViewOperationException e) {
            promise.reject(E_PRINT_ERROR, e);
        }*/

        // startActivityForResult(i, STATIC_INTEGER_VALUE);
      //  Intent i = new Intent(this._context, PrintActivity.class);
      //  i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
      //  getReactApplicationContext().startActivity(i);

    }

}