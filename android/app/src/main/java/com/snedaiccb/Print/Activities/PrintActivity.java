package com.snedaiccb.Print.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import com.snedaiccb.Print.Activities.Log.Loger;
import com.snedaiccb.R;
import com.za.finger.FingerHelper;
import com.za.finger.IUsbConnState;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import cn.pda.serialport.Tools;

public class PrintActivity extends AppCompatActivity  {


    private FingerHelper mFingerHelper ;  //option finger

    private int statues = 0 ;
    private int statuesUplooad = 0 ;
    private TextView statusInfos;
    private ImageView imgView;

    private long startTime = 0L ;
    private long endTime = 0L ;

    private String tag = "EnrolNewActivity" ;

    private Handler mHandler = new Handler(); //handle thread message

    private final int  IMAGE_SIZE = 256 * 288 ;//image size

    private String tempImgPath = "/mnt/sdcard/temp.bmp"  ;

    private Bitmap defaultBm  ;

    private int fpCharBuffer = 0 ;

    private int templateNum = 0 ;

    private String tips = "";

    private Resources res ;

    private String status = "0";
    private String signature = "";
    private String url = "";

    //IUsbConnState is to receive usb finger connect state
    private IUsbConnState usbConnstate = new IUsbConnState() {
        @Override
        public void onUsbConnected() {
            statues =  mFingerHelper.connectFingerDev() ;
            if (statues == mFingerHelper.CONNECT_OK) {
                tips = res.getString(R.string.conn_dev_success) ;
                setEmprunte();
            }else{
                tips = res.getString(R.string.conn_dev_fail) ;
                statusInfos.setText("Une erreur est survenue lors de la connexion au capteur USB, veuillez redémarrer l'appareil ");
            }
        }

        @Override
        public void onUsbPermissionDenied() {
            statusInfos.setText("Vous avez refusé l'accès au capteur USB");
        }

        @Override
        public void onDeviceNotFound() {
            Loger.e(tag, "onDeviceNotFound()");
            statusInfos.setText("Impossible de trouver le capteur USB, veuillez redémarrer l'appareil");
        }
    } ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_print);
        mFingerHelper = new FingerHelper(this, usbConnstate);
        res = this.getResources() ;
        statusInfos = (TextView) findViewById(R.id.statut_info);
      //  imgView = (ImageView) findViewById(R.id.img_fp);
        mFingerHelper.init();
    }

  protected void  setEmprunte(){
      statusInfos.setText("Placer votre doigt sur le Capteur USB");
      startTime = System.currentTimeMillis() ;
      endTime = startTime ;
      fpCharBuffer = mFingerHelper.CHAR_BUFFER_A ;
      //run match finger char task
      mHandler.postDelayed(enrollTask, 0);
  }


    @Override
    protected void onDestroy() {
        sendData();
        super.onDestroy();
    }

    @Override
    public void onBackPressed()
    {
        sendData();
    }

    protected void sendData(){
        Intent resultIntent = new Intent();
        resultIntent.putExtra("status", status);
        resultIntent.putExtra("signature", signature);
        resultIntent.putExtra("url", url);
        setResult(Activity.RESULT_OK, resultIntent);
        mHandler.removeCallbacksAndMessages(null);
        mFingerHelper.close();
        finish();
    }

    /**
     * get finger image task
     */
    private Runnable getFPImageTask = new Runnable() {
        @Override
        public void run() {
            String temp = ""  ;
            long timeCount = 0L ;
            endTime = System.currentTimeMillis() ;
            timeCount = endTime - startTime ;
            //search finger time 10s
            if (timeCount > 10000) {
                temp = res.getString(R.string.get_finger_img_time_out);
                statusInfos.setText(temp);
             //   setAllBtnEnable(true, btnOpen, false);
                return ;
            }
            statues = mFingerHelper.getImage() ;
            //find finger
            if (statues == mFingerHelper.PS_OK) {
                int[] recvLen = {0, 0};
                byte[] imageByte = new byte[IMAGE_SIZE];//256*288
                mFingerHelper.uploadImage(imageByte, recvLen);
                //switch to bmp
                mFingerHelper.imageData2BMP(imageByte, tempImgPath);
                temp = res.getString(R.string.get_finger_img_success);
                statusInfos.setText(temp);

              //  setAllBtnEnable(true, btnOpen, false);
            } else if (statues == mFingerHelper.PS_NO_FINGER) {
                temp = res.getString(R.string.searching_finger) + " ,time:" +((10000-(endTime - startTime)))/1000 +"s";
                statusInfos.setText(temp);
                mHandler.postDelayed(getFPImageTask, 100);
            } else if (statues == mFingerHelper.PS_GET_IMG_ERR) {
                temp = res.getString(R.string.get_img_error);
                statusInfos.setText(temp);
                mHandler.postDelayed(getFPImageTask, 100);
                return ;
            }else{
                temp = res.getString(R.string.dev_error);
                statusInfos.setText(temp);
                return ;
            }

        }


    } ;


    /**
     * get finger char
     */
    private Runnable getCharTask = new Runnable() {
        @Override
        public void run() {
            String temp = ""  ;
            long timeCount = 0L ;
            endTime = System.currentTimeMillis() ;
            timeCount = endTime - startTime ;
            //search finger time 10s
            if (timeCount > 10000) {
                temp = res.getString(R.string.get_finger_img_time_out);
             //   editTips.setText(temp);
             //   setAllBtnEnable(true, btnOpen, false);
                return ;
            }
            statues = mFingerHelper.getImage() ;
            //find finger
            if (statues == mFingerHelper.PS_OK) {
                //gen char to bufferA
                statues = mFingerHelper.genChar(mFingerHelper.CHAR_BUFFER_A);
                if (statues == mFingerHelper.PS_OK) {
                    int[] iCharLen = {0, 0} ;
                    byte[] charBytes = new byte[512];
                    //upload char
                    statues = mFingerHelper.upCharFromBufferID(mFingerHelper.CHAR_BUFFER_A, charBytes, iCharLen);
                    if (statues == mFingerHelper.PS_OK) {
                        //upload success
                        temp = res.getString(R.string.get_finger_char_success) +":\r\n " + Tools.Bytes2HexString(charBytes, 512);
                     //   editTips.setText(temp);
                     //   setAllBtnEnable(true, btnOpen, false);
                    }
                }else{
                    //char is bad quickly
                    temp = res.getString(R.string.finger_char_is_bad_try_again);
                  //  editTips.setText(temp);
                 //   setAllBtnEnable(true, btnOpen, false);
                    return ;
                }
            } else if (statues == mFingerHelper.PS_NO_FINGER) {
                temp = res.getString(R.string.searching_finger) + " ,time:" +((10000-(endTime - startTime)))/1000 +"s";
              //  editTips.setText(temp);
                mHandler.postDelayed(getCharTask, 100);
            } else if (statues == mFingerHelper.PS_GET_IMG_ERR) {
                temp = res.getString(R.string.get_img_error);
              //  editTips.setText(temp);
                mHandler.postDelayed(getCharTask, 100);
                return ;
            }else{
                temp = res.getString(R.string.dev_error);
              //  editTips.setText(temp);
              //  setAllBtnEnable(true, btnOpen, false);
                return ;
            }
        }
    } ;

    /**
     * match two finger char, if match score > 60 is the same finger
     */
    private Runnable matchFingerTask = new Runnable() {
        @Override
        public void run() {
            String temp = ""  ;
            long timeCount = 0L ;
            endTime = System.currentTimeMillis() ;
            timeCount = endTime - startTime ;
            //search finger time 10s
            if (timeCount > 10000) {
                temp = res.getString(R.string.get_finger_img_time_out);
              //  editTips.setText(temp);
              //  setAllBtnEnable(true, btnOpen, false);
                return ;
            }
            statues = mFingerHelper.getImage() ;
            //find finger
            if (statues == mFingerHelper.PS_OK) {
                //first finger
                if (fpCharBuffer == mFingerHelper.CHAR_BUFFER_A) {
                    //gen char to bufferA
                    statues = mFingerHelper.genChar(fpCharBuffer);
                    if (statues == mFingerHelper.PS_OK) {
                        temp = res.getString(R.string.gen_finger_buffer_a_press_again);
                      //  editTips.setText(temp);
                        fpCharBuffer = mFingerHelper.CHAR_BUFFER_B ;
                        mHandler.postDelayed(matchFingerTask, 2000);
                    }
                } else if (fpCharBuffer == mFingerHelper.CHAR_BUFFER_B) { //second finger
                    //gen char to bufferB
                    statues = mFingerHelper.genChar(fpCharBuffer);
                    if (statues == mFingerHelper.PS_OK) {
                        temp = res.getString(R.string.gen_finger_buffer_b_success) + " \r\n";
                      //  editTips.setText(temp);
                        //match buffer_a with buffer_b
                        int[] iScore = {0, 0} ;
                        mFingerHelper.match(iScore);
                        temp = res.getString(R.string.match_a_b_success_score) + " = " + iScore[0] ;
                       // editTips.append(temp);

                    }
                }
               // setAllBtnEnable(true, btnOpen, false);
            } else if (statues == mFingerHelper.PS_NO_FINGER) {
                temp = res.getString(R.string.searching_finger) + " ,time:" +((10000-(endTime - startTime)))/1000 +"s";
              //  editTips.setText(temp);
                mHandler.postDelayed(matchFingerTask, 100);
            } else if (statues == mFingerHelper.PS_GET_IMG_ERR) {
                temp = res.getString(R.string.get_img_error);
               // editTips.setText(temp);
               // setAllBtnEnable(true, btnOpen, false);
                return ;
            }else{
                temp = res.getString(R.string.dev_error);
             //   editTips.setText(temp);
             //   setAllBtnEnable(true, btnOpen, false);
                return ;
            }
        }
    }  ;


    /**
     * enroll finger char to flash database
     */
    private Runnable enrollTask  = new Runnable() {
        @Override
        public void run() {
            String temp = ""  ;
            long timeCount = 0L ;
            endTime = System.currentTimeMillis() ;
            timeCount = endTime - startTime ;
            //search finger time 10s
            if (timeCount > 10000) {
                temp = res.getString(R.string.get_finger_img_time_out);
                statusInfos.setText(temp);
               // setAllBtnEnable(true, btnOpen, false);
                return ;
            }
            statues = mFingerHelper.getImage() ;
            //find finger
            if (statues == mFingerHelper.PS_OK) {
                //first finger
                if (fpCharBuffer == mFingerHelper.CHAR_BUFFER_A) {
                    //gen char to bufferA
                    statues = mFingerHelper.genChar(fpCharBuffer);
                    if (statues == mFingerHelper.PS_OK) {
                        int[] iMaddr = {0, 0} ;
                        //is exist flash database,database size = 512
                        statues = mFingerHelper.search(mFingerHelper.CHAR_BUFFER_A, 0, 512, iMaddr);
                        if (statues == mFingerHelper.PS_OK) {

                            int[] iCharLen = {0, 0} ;
                            byte[] charBytes = new byte[512];
                            //upload char
                            statuesUplooad = mFingerHelper.upCharFromBufferID(mFingerHelper.CHAR_BUFFER_A, charBytes, iCharLen);
                            if (statuesUplooad == mFingerHelper.PS_OK) {
                               // statusInfos.setText(Tools.Bytes2HexString(charBytes, 512));
                                status = "1";
                                signature = Tools.Bytes2HexString(charBytes, 512);
                            }


                         //   temp = res.getString(R.string.already_exist_flash) + " , User id index["+ iMaddr[0] +"]";
                         //   statusInfos.setText(temp);
                         //   setAllBtnEnable(true, btnOpen, false);
                            sendData();
                            return ;
                        }
                        temp = res.getString(R.string.gen_finger_buffer_a_press_again);
                        statusInfos.setText(temp);
                        fpCharBuffer = mFingerHelper.CHAR_BUFFER_B ;
                        mHandler.postDelayed(enrollTask, 2000);
                    }
                } else if (fpCharBuffer == mFingerHelper.CHAR_BUFFER_B) { //second finger
                    //gen char to bufferB
                    statues = mFingerHelper.genChar(fpCharBuffer);
                    if (statues == mFingerHelper.PS_OK) {
                        temp = res.getString(R.string.gen_char) + " \r\n";
                        statusInfos.setText(temp);
                        //merge BUFFER_A with BUFFER_B , gen template to MODULE_BUFFER
                        mFingerHelper.regTemplate() ;
                        int[] iMbNum = {0, 0} ;
                        mFingerHelper.getTemplateNum(iMbNum);
                        templateNum = iMbNum[0];
                        if (templateNum >= 512) {
                            temp = res.getString(R.string.flash_database_full) + " \r\n";
                            statusInfos.setText(temp);
                           // setAllBtnEnable(true, btnOpen, false);
                            return ;
                        }
                        //store template to flash database
                        statues =  mFingerHelper.storeTemplate(mFingerHelper.MODEL_BUFFER, templateNum);
                        if (statues == mFingerHelper.PS_OK) {
                          //  temp = res.getString(R.string.enroll_success) + ", User id index["+templateNum +"] \r\n";
                          //  statusInfos.setText(temp);

                            int[] iCharLen = {0, 0} ;
                            byte[] charBytes = new byte[512];
                            //upload char
                            statuesUplooad = mFingerHelper.upCharFromBufferID(mFingerHelper.CHAR_BUFFER_B, charBytes, iCharLen);
                            if (statuesUplooad == mFingerHelper.PS_OK) {
                                status = "1";
                                signature = Tools.Bytes2HexString(charBytes, 512);
                            }
                            sendData();
                        }else{
                            temp = res.getString(R.string.enroll_fail) + ",statues= " + statues +" \r\n";
                            statusInfos.setText(temp);
                        }

                    }
                   // setAllBtnEnable(true, btnOpen, false);
                }

            } else if (statues == mFingerHelper.PS_NO_FINGER) {
                temp = res.getString(R.string.searching_finger) + " ,time:" +((10000-(endTime - startTime)))/1000 +"s";
                statusInfos.setText(temp);
                mHandler.postDelayed(enrollTask, 100);
            } else if (statues == mFingerHelper.PS_GET_IMG_ERR) {
                temp = res.getString(R.string.get_img_error);
                statusInfos.setText(temp);
             //   setAllBtnEnable(true, btnOpen, false);
                return ;
            }else{
                temp = res.getString(R.string.dev_error);
                statusInfos.setText(temp);
             //   setAllBtnEnable(true, btnOpen, false);
                return ;
            }
        }
    } ;

    /**
     * search finger in flash database
     */
    private Runnable searchTask = new Runnable() {
        @Override
        public void run() {
            String temp = ""  ;
            long timeCount = 0L ;
            endTime = System.currentTimeMillis() ;
            timeCount = endTime - startTime ;
            //search finger time 10s
            if (timeCount > 10000) {
                temp = res.getString(R.string.get_finger_img_time_out);
              //  editTips.setText(temp);
               // setAllBtnEnable(true, btnOpen, false);
                return ;
            }
            statues = mFingerHelper.getImage() ;
            //find finger
            if (statues == mFingerHelper.PS_OK) {
                //gen char to bufferA
                statues = mFingerHelper.genChar(mFingerHelper.CHAR_BUFFER_A);
                if (statues == mFingerHelper.PS_OK) {
                    int[] iMaddr = {0, 0} ;
                    //is exist flash database,database size = 512
                    statues = mFingerHelper.search(mFingerHelper.CHAR_BUFFER_A, 0, 512, iMaddr);
                    if (statues == mFingerHelper.PS_OK) {
                        temp = res.getString(R.string.finger_is_found) +" , User id index["+ iMaddr[0] +"]";
                      //  editTips.setText(temp);

                    }else{
                        temp = res.getString(R.string.no_found_finger_in_flash);
                      //  editTips.setText(temp);
                    }
                   // setAllBtnEnable(true, btnOpen, false);
                }
            } else if (statues == mFingerHelper.PS_NO_FINGER) {
                temp = res.getString(R.string.searching_finger) + " ,time:" +((10000-(endTime - startTime)))/1000 +"s";
              //  editTips.setText(temp);
                mHandler.postDelayed(searchTask, 100);
            } else if (statues == mFingerHelper.PS_GET_IMG_ERR) {
                temp = res.getString(R.string.get_img_error);
               // editTips.setText(temp);
              //  setAllBtnEnable(true, btnOpen, false);
                return ;
            }else{
                temp = res.getString(R.string.dev_error);
             //   editTips.setText(temp);
              //  setAllBtnEnable(true, btnOpen, false);
                return ;
            }
        }
    } ;


}