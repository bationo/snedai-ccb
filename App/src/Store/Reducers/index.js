import {combineReducers} from 'redux';
import locking from './locking.reducer';
import enrolementData from './enrol.reducer';

export default combineReducers({
  locking,
  enrolementData,
});
