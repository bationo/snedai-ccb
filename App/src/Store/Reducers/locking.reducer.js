import {
    LOCKING_ENUM
} from "../../Shared/Utils/InputEnum";

const initialState: any = {
    visible: false,
    message: "",
}

function locking(state = initialState, action) {
    let nextState: any = initialState;
    switch (action.type) {
        case LOCKING_ENUM.SET_INFOS:
            nextState = action.value;
            return nextState || state
        default:
            return state
    }
}

export default locking