import {ENROL_MODEL} from '../../Shared/Utils/InputEnum';

import {EnrolModel, EmprunteClass} from '../../Shared/Models/enrol.model';

const initialState: EnrolModel = {
  natureDemande: null,
  numeroRegistre: null,
  dateDemande: new Date(),
  nom: null,
  prenom: null,
  profession: null,
  dateNaissance: new Date(),
  localiteOrigine: null,
  nomPrenomPere: null,
  dateNaissancePere: new Date(),
  dateNaissanceMere: new Date(),
  lieuResidence: null,
  adresse: null,
  telephone: null,
  signeParticulier: null,
  taille: null,
  teint: null,
  sexe: null,
  groupeSanguin: null,
  personneContact: null,
  typeDocument: null,
  numeroPieceFournie: null,
  nomAutoritePieceFournie: null,
  codeDelegue: null,
  nomDelegue: null,
  nomPrenomMere: null,
  photo: null,
  photoUrl: null,
  emprunte: [
    {id: 0, libelle: null, status: false},
    {id: 1, libelle: null, status: false},
    {id: 2, libelle: null, status: false},
    {id: 3, libelle: null, status: false},
    {id: 4, libelle: null, status: false},
    {id: 5, libelle: null, status: false},
    {id: 6, libelle: null, status: false},
    {id: 7, libelle: null, status: false},
    {id: 8, libelle: null, status: false},
    {id: 9, libelle: null, status: false},
  ],
};

function enrolementData(state = initialState, action) {
  let nextState: EnrolModel = initialState;
  switch (action.type) {
    case ENROL_MODEL.SET_INFOS:
      nextState = action.value;
      return nextState || state;
    default:
      return state;
  }
}

export default enrolementData;
