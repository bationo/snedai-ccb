import React from 'react';

import {
  createAppContainer,
  createDrawerNavigator,
  createStackNavigator,
  createSwitchNavigator,
} from 'react-navigation';

import MainComponent from './Components/main.component';
import SideBarComponent from './Shared/Components/SideBar';
import EnrolComponent from './Components/Enrol/enrol.component';
import ListEnrolComponent from './Components/ListEnrol/listEnrol.component';

const Main = createDrawerNavigator(
  {
    home: {
      screen: MainComponent,
    },
  },
  {
    navigationOptions: {},
    contentComponent: props => <SideBarComponent {...props} />,
  },
);

const App = createStackNavigator(
  {
    main: Main,
    enrol: EnrolComponent,
    listEnrol: ListEnrolComponent,
  },
  {
    headerMode: 'none',
  },
);

const navigation = createSwitchNavigator(
  {
    app: App,
  },
  {
    initialRouteName: 'app',
  },
);
const AppContainer = createAppContainer(navigation);
export default AppContainer;
