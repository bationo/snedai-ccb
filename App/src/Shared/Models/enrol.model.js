export interface EnrolModel {
  natureDemande: string;
  numeroRegistre: string;
  dateDemande: Date;
  nom: string;
  prenom: string;
  profession: string;
  dateNaissance: Date;
  localiteOrigine: string;
  nomPrenomPere: string;
  nomPrenomMere: string;
  dateNaissancePere: Date;
  dateNaissanceMere: Date;
  lieuResidence: string;
  adresse: string;
  telephone: string;
  signeParticulier: string;
  taille: string;
  teint: string;
  sexe: string;
  groupeSanguin: string;
  personneContact: string;
  typeDocument: string;
  numeroPieceFournie: string;
  nomAutoritePieceFournie: string;
  codeDelegue: string;
  nomDelegue: string;
  emprunte: EmprunteModel[];
  photo: any;
  photoUrl: string;
}

export interface EmprunteModel {
  id: Number;
  libelle: String;
  status: Boolean;
}

export class EmprunteClass {
  libelle: String;
  status: Boolean;
}
