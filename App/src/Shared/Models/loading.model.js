export interface LoadingModel {
  visible: boolean;
  message: string;
}