export const INPUT_ENUM = {
  EMAIL: 'email',
  PASSWORD: 'password',
  PASSWORD_NO_BLANK: 'password_no_blank',
  PASSWORD_CONFIRM: 'passwordConfirm',
  PIN_CODE: 'codePin',
  NO_BLANK: 'no_blank',
  SOLDE_INFERIEUR: 'solde_inferieur',
  RESTANT_NONNAIE: 'restant_monnaie',
  ID: 'id',
  NAME: 'name',
  SOLDE: 'solde',
  PASSWORD_VERIF: 'passwordVerif',
  LIMITE: 'limite',
  LASTNAME: 'lastName',
  IMEI: 'imei',
};
export const ENROL_MODEL = {
  SET_INFOS: 'new_enrol',
};
export const LOCKING_ENUM = {
  SET_INFOS: 'set_locking_infos',
};
