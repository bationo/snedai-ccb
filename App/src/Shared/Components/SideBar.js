import React from 'react';
import {Text, View, StatusBar} from 'react-native';
import {
  Container,
  Content,
  Left,
  Right,
  List,
  Header,
  Body,
  Button,
} from 'native-base';
import appStyle from '../Styles/app';
import {connect} from 'react-redux';

const styleComponent = appStyle.getStyle();

class SideBarComponent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      modal: false,
    };
  }
  toggleMenus() {
    this.props.navigation.toggleDrawer();
  }

  render() {
    return (
      <Container>
        <Header
          style={{
            backgroundColor: '#239D6E',
            height: 70,
            elevation: 0,
            borderWidth: 0,
          }}>
          <StatusBar backgroundColor="#000051" barStyle="light-content" />
          <Left>
            <Button onPress={() => this.home()} transparent>
              <View style={{width: 100}}>
                <Text
                  style={{
                    color: '#FFFFFF',
                    fontWeight: 'bold',
                    fontSize: 30,
                  }}>
                  Bifree
                </Text>
              </View>
            </Button>
          </Left>
          <Body />
          <Right />
        </Header>
        <Content>
          <List transparent style={{marginTop: 10}} />
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
export default connect(mapStateToProps)(SideBarComponent);
