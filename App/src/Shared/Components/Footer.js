import React from "react";
import { Footer, FooterTab, Button, Icon } from "native-base";
import { withNavigation } from "react-navigation";

import appStyle from "../Styles/app";

const styleComponent = appStyle.getStyle();

class FooterApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      etat: false
    };
  }

  toggleMenus() {
    this.props.navigation.toggleDrawer();
  }

  render() {
    return (
      <Footer>
        <FooterTab style={{ backgroundColor: "#00AC7B" }}>
          <Button onPress={() => this.props.navigation.navigate("share", {})}>
            <Icon name="share" style={{ color: "white", fontSize: 35 }} />
          </Button>
          <Button onPress={() => this.props.navigation.navigate("notifs")}>
            <Icon
              name="notifications"
              style={{ color: "white", fontSize: 35 }}
            />
          </Button>
          <Button onPress={() => this.toggleMenus()}>
            <Icon active name="menu" style={{ color: "white", fontSize: 35 }} />
          </Button>
        </FooterTab>
      </Footer>
    );
  }
}

export default withNavigation(FooterApp);
