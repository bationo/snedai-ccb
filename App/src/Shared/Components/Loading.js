import React from "react";
import {
  Text,
  ActivityIndicator,
  Dimensions,
  ImageBackground
} from "react-native";

import { View } from "native-base";
import LoadingModel from "../Models/loading.model";
import appStyle from "../Styles/app";
import Modal from "react-native-modal";

const styleComponent = appStyle.getStyle();
const { height, width } = Dimensions.get("window");

export default class Loading extends React.Component {
  loadding: LoadingModel;

  constructor(props) {
    super(props);
    this.loadding = {
      visible: this.props.visible,
      message: this.props.message
    };
    this.state = {
      etat: true,
      visible: false
    };
  }

  componentWillUpdate(nextProps, nextState) {
    this.loadding = nextProps;
  }

  render() {
    return (
      <View>
        {this.loadding.visible && (
          <ImageBackground
            style={[styleComponent.imageBackground, { height }]}
            source={require("../../images/BG_autre.png")}
          >
            <Modal
              transparent={true}
              isVisible={this.loadding.visible}
              backdropColor={"white"}
              backdropOpacity={0}
              animationIn="zoomInDown"
              animationOut="zoomOutUp"
              animationInTiming={1000}
              animationOutTiming={1000}
              backdropTransitionInTiming={1000}
              backdropTransitionOutTiming={1000}
              style={{ height, width: "100%" }}
            >
              <View
                style={[
                  styleComponent.containerConfirmation,
                  { height: height / 2 }
                ]}
              >
                <View style={styleComponent.justifyContent}>
                  <View style={{ width: 200 }}>
                    <Text style={styleComponent.textModal}>
                      {this.loadding.message}
                    </Text>
                    <ActivityIndicator size="large" color="#F7941E" />
                  </View>
                </View>
              </View>
            </Modal>
          </ImageBackground>
        )}
      </View>
    );
  }
}
