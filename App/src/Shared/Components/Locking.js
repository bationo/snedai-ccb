import React from "react";
import { ImageBackground, Dimensions, ToastAndroid } from "react-native";
import { View } from "native-base";
import LockingModel from "../Models/locking.model";
import UserModel from "../Models/user.model";
import appStyle from "../Styles/app";
import Modal from "react-native-modal";
import PinView from "react-native-pin-view";
import { LOCKING_ENUM } from "../Utils/InputEnum";
import { connect } from "react-redux";

const styleComponent = appStyle.getStyle();
const { height, width } = Dimensions.get("window");

class Locking extends React.Component {
  locking: LockingModel;
  user: UserModel;

  constructor(props) {
    super(props);
    this.onComplete = this.onComplete.bind(this);
    this.locking = this.props.locking;
    this.user = this.props.currentUser;
    this.state = {
      etat: 0,
      visible: false
    };
  }

  toastShow() {
    ToastAndroid.show("le code pin est incorrect", ToastAndroid.SHORT);
  }

  refresh() {
    this.setState({ etat: this.state.etat++ });
  }

  lockingApp() {
    let locking = this.props.locking;
    locking.visible = false;
    const action = { type: LOCKING_ENUM.SET_INFOS, value: locking };
    this.props.dispatch(action);
    this.refresh();
  }

  onComplete(inputtedPin, clear) {
    if (inputtedPin !== this.props.currentUser.codePin) {
      clear();
      this.toastShow();
    } else {
      this.lockingApp();
    }
  }

  render() {
    return (
      <View>
        <Modal isVisible={this.locking.visible} transparent={true}>
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <ImageBackground
              style={[styleComponent.imageBackground, { height, width }]}
              source={require("../../images/BG_look.png")}
            >
              <View style={{ marginTop: 100 }}>
                <View style={styleComponent.justifyContent}>
                  <PinView
                    onComplete={this.onComplete}
                    pinLength={this.props.currentUser.codePin.length}
                  />
                </View>
              </View>
            </ImageBackground>
          </View>
        </Modal>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    locking: state.locking,
    currentUser: state.currentUser
  };
};
export default connect(mapStateToProps)(Locking);
