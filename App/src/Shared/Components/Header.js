import React from "react";
import {
  Header,
  Left,
  Button,
  Icon,
  Body,
  Title,
  Right,
  View,
  Text
} from "native-base";
import { Image, StatusBar } from "react-native";
import { withNavigation } from "react-navigation";
import { connect } from "react-redux";

import appStyle from "../Styles/app";

const styleComponent = appStyle.getStyle();

class HeaderApp extends React.Component {
  title = "";

  constructor(props) {
    super(props);
    this.title = this.props.title;
    this.state = {
      etat: false
    };
  }

  toggleMenus() {
    this.props.navigation.toggleDrawer();
  }

  home() {
    this.props.navigation.closeDrawer();
    this.props.navigation.navigate("home", {});
  }

  render() {
    return (
      <Header
        style={{
          backgroundColor: "#239D6E",
          height: 70,
          elevation: 0,
          borderWidth: 0
        }}
        androidStatusBarColor="#1a237e"
      >
        <StatusBar backgroundColor="#1a237e" barStyle="light-content" />
        <View style={{ flex: 1, flexDirection: "row", top: 10 }}>
          <View style={{ width: "30%" }}>
            {this.props.title.title != "" && (
              <View>
                <Button transparent>
                  <Icon
                    active
                    name="home"
                    onPress={() => this.home()}
                    style={{ color: "white", fontSize: 30, left: -10 }}
                  />
                  <Image
                    onPress={() => this.home()}
                    style={{ width: 90, height: 30, left: -20 }}
                    source={require("../../images/logosw.png")}
                  />
                </Button>
              </View>
            )}
            {!this.props.title.title != "" && (
              <Button transparent onPress={() => this.home()}>
                <Image
                  style={{ width: 110, height: 40, left: 8 }}
                  source={require("../../images/logo-white.png")}
                />
              </Button>
            )}
          </View>
          <View style={{ width: "50%" }}>
            <Text
              style={{
                textAlign: "center",
                color: "white",
                fontWeight: "bold",
                fontSize: 15,
                top: 12
              }}
            >
              {this.props.title.title}
            </Text>
          </View>
          <View
            style={{ width: "20%", flex: 1, flexDirection: "row", left: -25 }}
          >
            <View>
              <Button transparent>
                <Icon
                  active
                  name="notifications"
                  onPress={() => this.props.navigation.navigate("notifs", {})}
                  style={{ color: "white", fontSize: 35, left: 5 }}
                />
              </Button>
            </View>
            <View>
              <Button transparent onPress={() => this.toggleMenus()}>
                <Icon
                  active
                  name="menu"
                  style={{ color: "white", fontSize: 35, left: -10 }}
                />
              </Button>
            </View>
          </View>
        </View>
      </Header>
    );
  }
}
const mapStateToProps = state => {
  return {
    title: state.title,
    agences: state.agences,
    gabs: state.gabs,
    ville: state.ville,
    searchInput: state.searchInput,
    mapDisplay: state.mapDisplay
  };
};
export default withNavigation(connect(mapStateToProps)(HeaderApp));
