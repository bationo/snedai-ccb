export const ListEnrolComponentStyle = new (class StyleProvider {
  getStyle() {
    return {
      containerModal: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
      },
      colorB: {
        color: "#454F63"
      },
      modalBloc: {
        height: 320,
        width: 320,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        shadowOffset: {
          width: 20,
          height: 20
        },
        shadowColor: "black",
        shadowOpacity: 2,
        elevation: 3,
        backgroundColor: "white",
        borderRadius: 10
      },
      containers: {
        backgroundColor: "white"
      },
      imageBackground: {
        width: "100%"
      },
      Input: {},
      image: {
        height: 90,
        width: 71
      },
      content: {
        marginTop: 0
      },
      contentRegister: {
        marginTop: 5
      },
      margin5: {
        marginTop: 5
      },
      margin20: {
        margin: 20
      },
      marginTop20: {
        marginTop: 20
      },
      justifyContent: {
        justifyContent: "center",
        alignItems: "center"
      },
      buttonSection: {
        flex: 1,
        alignItems: "center"
      },
      icon: {
        color: "#A4A4A4",
        top: 30,
        alignSelf: "flex-end",
        position: "absolute",
        right: 15
      },
      passordVisible: {
        top: -50,
        alignSelf: "flex-end"
      },
      color1: {
        color: "#A4A4A4"
      },
      color2: {
        color: "#ffffff"
      },
      label: {
        color: "#A4A4A4",
        fontWeight: "bold"
      },
      placeholderTextColor: {
        color: "#DCDCDC"
      },
      footerContainer: {
        justifyContent: "center",
        alignItems: "center",
        width: "100%"
      },
      button: {
        width: 250,
        height: 60,
        alignSelf: "center"
      },
      bgWhite: {
        backgroundColor: "white"
      },
      buttonTransparent: {
        backgroundColor: "#F7941E",
        borderColor: "white",
        borderWidth: 1,
        marginTop: 20
      },
      buttonTransparentRegister: {
        backgroundColor: "#F7941E",
        marginTop: 20
      },
      textButton: {
        fontWeight: "bold",
        fontSize: 18
      },
      colorPrimary: {
        color: "#F7941E"
      },
      colorWhite: {
        color: "white"
      },
      password: {
        marginTop: 10,
        alignSelf: "flex-end",
        color: "white",
        fontSize: 14
      },
      title: {
        textAlign: "center",
        marginTop: 10,
        color: "#F7941E",
        fontWeight: "bold"
      },
      confirmPassword: {
        position: "absolute",
        top: 110
      },
      top50: {
        top: -50
      },
      textConfirmation: {
        textAlign: "center",
        color: "#A4A4A4",
        width: 250,
        marginTop: 20
      },
      containerConfirmation: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
      },
      textCenter: {
        textAlign: "center"
      },
      bold: {
        fontWeight: "bold"
      },
      soldeItem: {
        borderRadius: 8,
        backgroundColor: "#F6F6F6",
        textAlign: "center",
        borderColor: "#A4A4A4"
      },
      soldeInput: {
        textAlign: "center",
        fontWeight: "bold",
        color: "#888888"
      }
    };
  }
})();
