import React from 'react';
import {Dimensions, Image, Alert, StatusBar} from 'react-native';
import {
  Container,
  Content,
  Text,
  Item,
  Input,
  Icon,
  Button,
  Header,
  Body,
  Left,
  Right,
  H2,
  View,
  List,
  ListItem,
} from 'native-base';
import {ListEnrolComponentStyle} from './listEnrol.style';
import {connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import SInfo from 'react-native-sensitive-info';
import Table from 'react-native-simple-table';

const styleComponent = ListEnrolComponentStyle.getStyle();
const {height} = Dimensions.get('window');

const columns = [
  {
    title: 'Nom',
    dataIndex: 'nom',
  },
  {
    title: 'Prenom',
    dataIndex: 'prenom',
  },
  {
    title: 'Sexe',
    dataIndex: 'sexe',
  },
  {
    title: 'Tel',
    dataIndex: 'telephone',
  },
  {
    title: 'Residence',
    dataIndex: 'lieuResidence',
    width: 140
  },
];

class ListEnrolComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      confirm: false,
      datasEnrol: [],
    };
  }
  reset() {
    this.setState({confirm: false});
  }

  loadData() {
    SInfo.getItem('dataEnrol', {}).then(value => {
      if (value != null) {
        this.state.datasEnrol = JSON.parse(value);
        this.refresh();
        console.log(this.state.datasEnrol);
      }
    });
  }

  componentDidMount() {
    this.loadData();
  }

  refresh() {
    this.setState({refresh: true});
  }

  render() {
    return (
      <Container>
        <Spinner visible={this.state.load} textContent={''} />
        <Header
          style={{
            backgroundColor: '#1a237e',
            height: 70,
          }}>
          <StatusBar backgroundColor="#000051" barStyle="light-content" />
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon style={styleComponent.color2} name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                color: 'white',
                fontSize: 18,
                right: 50,
                fontWeight: 'bold',
              }}>
              Liste des Enrôlements
            </Text>
          </Body>
        </Header>
        <Content>
          <View style={{padding: 10}}>
            <Table
              height={320}
              columns={columns}
              dataSource={this.state.datasEnrol}
            />
          </View>
        </Content>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  return {};
};
export default connect(mapStateToProps)(ListEnrolComponent);
