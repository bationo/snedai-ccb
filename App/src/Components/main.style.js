export const MainComponentStyle = new (class StyleProvider {
  getStyle() {
    return {
      containerModal: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
      },
      modalBloc: {
        height: 320,
        width: 320,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        shadowOffset: {
          width: 20,
          height: 20
        },
        shadowColor: "black",
        shadowOpacity: 2,
        elevation: 3,
        backgroundColor: "white",
        borderRadius: 10
      },
      textCenter: {
        textAlign: "center"
      },
      squares: {
        width: 130,
        height: 90,
        backgroundColor: "#FFFFFF",
        borderRadius: 12,
        borderWidth: 0,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        margin: 5,
        padding: 5,
        shadowColor: "#455B63",
        shadowOffset: {
          width: 0,
          height: 4
        },
        shadowOpacity: 0.36,
        shadowRadius: 6.68,
        elevation: 1
      },
      squaresParent: {
        marginTop: 40,
        padding: 10,
        flexWrap: "wrap",
        alignItems: "flex-start",
        flexDirection: "row",
        justifyContent: "center"
      },
      squaresView: {
        textAlign: "center",
        fontSize: 13,
        color: "#1a237e",
        fontWeight: "bold"
      },
      squareIcon: {
        width: 40,
        height: 40
      },
      squareIcon1: {
        width: 40,
        height: 40
      },
      tab: {
        top: 22
      },
      bg: {
        backgroundColor: "#FFFFFF"
      },
      colorBlack: {
        color: "#959DAD"
      },
      bold: {
        fontWeight: "bold"
      },
      square: {
        width: 130,
        height: 130,
        backgroundColor: "white",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        margin: 5,
        padding: 5,
        shadowOffset: {
          width: 20,
          height: 20
        },
        shadowColor: "black",
        shadowOpacity: 2,
        elevation: 3,
        backgroundColor: "white"
      },
      buttonContainer: {
        backgroundColor: "#2E9298",
        borderRadius: 10,
        padding: 10,
        shadowOffset: {
          width: 0,
          height: 13
        },
        shadowOpacity: 0.3,
        shadowRadius: 6,
        elevation: 3
      },
      square1: {
        width: 100,
        height: 100,
        backgroundColor: "white",
        borderRadius: 4,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        margin: 5,
        padding: 5
      },
      padding: {
        padding: 10,
        flexWrap: "wrap",
        alignItems: "flex-start",
        flexDirection: "row",
        justifyContent: "center",
        marginTop: 60
      },
      imageIcon: {
        width: 70,
        height: 70
      },
      containers: {
        backgroundColor: "white"
      },
      imageBackground: {
        width: "100%",
        height: "100%"
      },
      Input: {},
      image: {
        height: 90,
        width: 71
      },
      content: {
        marginTop: 50
      },
      contentRegister: {
        marginTop: 5
      },
      margin20: {
        margin: 20
      },
      marginTop20: {
        marginTop: 20
      },
      justifyContent: {
        justifyContent: "center",
        alignItems: "center"
      },
      buttonSection: {
        flex: 1,
        alignItems: "center"
      },
      icon: {
        top: 30,
        alignSelf: "flex-end",
        position: "absolute",
        right: 15
      },
      passordVisible: {
        top: -50,
        alignSelf: "flex-end"
      },
      color1: {
        color: "#A4A4A4"
      },
      fontSize18: {
        fontSize: 18
      },
      label: {
        color: "#A4A4A4",
        fontWeight: "bold"
      },
      placeholderTextColor: {
        color: "#DCDCDC"
      },
      footerContainer: {
        justifyContent: "center",
        alignItems: "center",
        width: "100%"
      },
      button: {
        width: 150,
        height: 40,
        alignSelf: "center"
      },
      bgWhite: {
        backgroundColor: "white"
      },
      buttonTransparent: {
        backgroundColor: "#F7941E",
        borderColor: "white",
        borderWidth: 1,
        marginTop: 20
      },
      buttonTransparentRegister: {
        backgroundColor: "#00AC7B",
        marginTop: 20
      },
      textButton: {
        fontWeight: "bold",
        fontSize: 16
      },
      colorPrimary: {
        color: "#F7941E"
      },
      colorWhite: {
        color: "white"
      },
      password: {
        marginTop: 10,
        alignSelf: "flex-end",
        color: "white",
        fontSize: 14
      },
      title: {
        textAlign: "center",
        marginTop: 10,
        color: "#F7941E",
        fontWeight: "bold"
      },
      confirmPassword: {
        position: "absolute",
        top: 110
      },
      top50: {
        top: -50
      },
      textConfirmation: {
        textAlign: "center",
        color: "#A4A4A4",
        width: 250,
        marginTop: 20
      },
      containerConfirmation: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center"
      },
      text: {
        fontSize: 14,
        textAlign: "center",
        color: "#F7941E",
        fontWeight: "bold",
        justifyContent: "flex-end",
        position: "absolute",
        bottom: 10
      }
    };
  }
})();
