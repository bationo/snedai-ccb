import React from 'react';
import {MainComponentStyle} from './main.style';
import {connect} from 'react-redux';
import {
  Image,
  Dimensions,
  Alert,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import {
  Container,
  Content,
  Button,
  Icon,
  Text,
  View,
  Card,
  Header,
  Left,
  Body,
  Right,
  CheckBox,
  ListItem,
} from 'native-base';
import {Col, Row, Grid} from 'react-native-easy-grid';
const styleComponent = MainComponentStyle.getStyle();
import Spinner from 'react-native-loading-spinner-overlay';

const {width, height} = Dimensions.get('window');
class MainComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      load: false,
    };
  }

  showMsg() {
    ToastAndroid.show(
      'Cette fonctionnalité sera bientot disponible',
      ToastAndroid.SHORT,
    );
  }
  render() {
    return (
      <Container style={{backgroundColor: '#F7F7FA'}}>
        <Spinner visible={this.state.load} textContent={''} />
        <Header
          style={{
            backgroundColor: '#1a237e',
            height: 70,
          }}>
          <Left />
          <Body>
            <Text
              style={{
                color: 'white',
                fontSize: 18,
                right: 50,
                fontWeight: 'bold',
              }}>
              BIENVENUE - SNEDAI CCB
            </Text>
          </Body>
        </Header>
        <Content>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 25,
              fontWeight: 'bold',
              marginTop: 20,
              color: '#1a237e',
            }}>
            Que voulez-vous faire ?
          </Text>
          <View style={{marginTop: 50}}>
            <Grid>
              <Col>
                <View style={{alignSelf: 'flex-end', right: 15}}>
                  <TouchableOpacity onPress={() => this.showMsg()}>
                    <View style={styleComponent.squares}>
                      <Image
                        style={styleComponent.squareIcon}
                        source={require('../images/group.png')}
                      />
                      <Text style={styleComponent.squaresView}>
                        Gestion des Users
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </Col>
              <Col>
                <View style={{alignSelf: 'flex-start', left: 20}}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('enrol')}>
                    <View style={styleComponent.squares}>
                      <Image
                        style={styleComponent.squareIcon}
                        source={require('../images/clipboardd.png')}
                      />
                      <Text style={styleComponent.squaresView}>
                        Nouveau Enrôl.
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </Col>
            </Grid>
            <Grid>
              <Col>
                <View style={{alignSelf: 'flex-end', right: 15, marginTop: 15}}>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('listEnrol')}>
                    <View style={styleComponent.squares}>
                      <Image
                        style={styleComponent.squareIcon}
                        source={require('../images/health_report.png')}
                      />
                      <Text style={styleComponent.squaresView}>
                        Historique Enrôl.
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </Col>
              <Col>
                <View
                  style={{alignSelf: 'flex-start', left: 20, marginTop: 15}}>
                  <TouchableOpacity onPress={() => this.showMsg()}>
                    <View style={styleComponent.squares}>
                      <Image
                        style={styleComponent.squareIcon}
                        source={require('../images/database.png')}
                      />
                      <Text style={styleComponent.squaresView}>
                        Synchro Serveur
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </Col>
            </Grid>
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = state => {
  return {};
};
export default connect(mapStateToProps)(MainComponent);
