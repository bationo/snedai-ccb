import React from 'react';
import {ALert, TouchableOpacity, Image, ToastAndroid} from 'react-native';
import {View, Button, Text, Icon} from 'native-base';
import {EnrolComponentStyle} from './enrol.style';
import {connect} from 'react-redux';
import {EnrolModel} from '../../Shared/Models/enrol.model';
import {ENROL_MODEL} from '../../Shared/Utils/InputEnum';
import ImagePicker from 'react-native-image-picker';

const styleComponent = EnrolComponentStyle.getStyle();

const options = {
  title: 'Sélectionnez image',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};

class ImageEnrol extends React.Component {
  formEnrolement: EnrolModel;
  constructor(props) {
    super(props);
    this.formEnrolement = this.props.enrolementData;
    this.state = {
      load: false,
      currentPosition: 0,
      loadEmpunte: false,
      title: 'Obtenir',
    };
  }
  refresh() {
    this.setState({refresh: true});
  }

  showMsg() {
    ToastAndroid.show(
      'Cette fonctionnalité sera bientot disponible',
      ToastAndroid.SHORT,
    );
  }

  selecImage() {
    ImagePicker.showImagePicker(options, response => {
      // console.log('Response = ', response);
      if (response.didCancel) {
        //  console.log('User cancelled image picker');
      } else if (response.error) {
        //  console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        //  console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        this.formEnrolement.photoUrl = source;
        this.formEnrolement.photo = response;
        this.refresh();
      }
    });
  }

  componentWillUnmount() {
    this.saveData();
  }

  saveData() {
    const action = {type: ENROL_MODEL.SET_INFOS, value: this.formEnrolement};
    this.props.dispatch(action);
  }

  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        {this.formEnrolement.photo != null && (
          <TouchableOpacity>
            <View style={styleComponent.blockPhoto}>
              <Image
                style={{width: '100%', height: '100%'}}
                source={this.formEnrolement.photo}
              />
            </View>
          </TouchableOpacity>
        )}
        <View>
          <Button
            iconLeft
            disabled={this.state.prev}
            style={{
              width: 160,
              marginTop: 30,
              backgroundColor: '#004d40',
            }}
            onPress={() => this.selecImage()}>
            <Icon name="reverse-camera" />
            <Text style={{left: -5, fontSize: 10}}>Lancer la camera</Text>
          </Button>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {enrolementData: state.enrolementData};
};
export default connect(mapStateToProps)(ImageEnrol);
