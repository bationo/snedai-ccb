import React from 'react';
import {ALert, TouchableOpacity, Image, ToastAndroid} from 'react-native';
import {View, Button, Text} from 'native-base';
import {EnrolComponentStyle} from './enrol.style';
import {connect} from 'react-redux';
import Print from '../../Shared/Components/Print';
import {EnrolModel} from '../../Shared/Models/enrol.model';
import {ENROL_MODEL} from '../../Shared/Utils/InputEnum';
const styleComponent = EnrolComponentStyle.getStyle();

class EmprunteEnrol extends React.Component {
  formEnrolement: EnrolModel;
  constructor(props) {
    super(props);
    this.formEnrolement = this.props.enrolementData;
    this.state = {
      load: false,
      currentPosition: 0,
      loadEmpunte: false,
      title: 'Obtenir',
    };
  }
  refresh() {
    this.setState({refresh: true});
  }

  _onPressButton(i) {
    if (this.formEnrolement.emprunte[i].status === false) {
      Print.navigateToExample().then(
        result => {
          if (result.status == 1) {
            this.formEnrolement.emprunte[i].status = true;
            this.formEnrolement.emprunte[i].libelle = result.signature;
            this.refresh();
          }
        },
        error => {},
      );
    } else {
      this.formEnrolement.emprunte[i].status = false;
      this.refresh();
    }
  }

  showMsg() {
    ToastAndroid.show(
      'Cette fonctionnalité sera bientot disponible',
      ToastAndroid.SHORT,
    );
  }

  componentWillUnmount() {
    this.saveData();
  }

  saveData() {
    const action = {type: ENROL_MODEL.SET_INFOS, value: this.formEnrolement};
    this.props.dispatch(action);
  }

  render() {
    return (
      <View style={{justifyContent: 'center', alignItems: 'center'}}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            flexWrap: 'wrap',
          }}>
          {this.formEnrolement.emprunte.map((emprunte, i) => {
            return (
              <TouchableOpacity key={i} onPress={() => this._onPressButton(i)}>
                <View style={styleComponent.blockEmprunte}>
                  {emprunte.status && (
                    <Image
                      style={styleComponent.squareIcon}
                      source={require('../../images/empreinte.jpg')}
                    />
                  )}
                  <Button
                    onPress={() => this._onPressButton(i)}
                    style={styleComponent.btnEmprunte}>
                    {emprunte.status && (
                      <Text style={styleComponent.btnTxt}>Supprimer</Text>
                    )}
                    {emprunte.status === false && (
                      <Text style={styleComponent.btnTxt}>Obtenir</Text>
                    )}
                  </Button>
                </View>
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {enrolementData: state.enrolementData};
};
export default connect(mapStateToProps)(EmprunteEnrol);
