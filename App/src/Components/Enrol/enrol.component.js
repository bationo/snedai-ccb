import React from 'react';
import {Dimensions, Image, Alert, StatusBar} from 'react-native';
import {
  Container,
  Content,
  Text,
  Item,
  Input,
  Icon,
  Button,
  Header,
  Body,
  Left,
  Right,
  H2,
  View,
  List,
  ListItem,
  Label,
} from 'native-base';
import {EnrolComponentStyle} from './enrol.style';
import {connect} from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import StepIndicator from 'react-native-step-indicator';
import FormEnrolComponent from './formEnrol.component';
import EmprunteEnrol from './emprunteEnrol.component';
import ImageEnrol from './imageEnrol.component';
import {EnrolModel} from '../../Shared/Models/enrol.model';
import {ENROL_MODEL} from '../../Shared/Utils/InputEnum';
import SInfo from 'react-native-sensitive-info';

const labels = ['Données signaletiques', 'Empreintes', 'Photo'];
const customStyles = {
  stepIndicatorSize: 25,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#fe7013',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#fe7013',
  stepStrokeUnFinishedColor: '#aaaaaa',
  separatorFinishedColor: '#fe7013',
  separatorUnFinishedColor: '#aaaaaa',
  stepIndicatorFinishedColor: '#fe7013',
  stepIndicatorUnFinishedColor: '#ffffff',
  stepIndicatorCurrentColor: '#ffffff',
  stepIndicatorLabelFontSize: 13,
  currentStepIndicatorLabelFontSize: 13,
  stepIndicatorLabelCurrentColor: '#fe7013',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#aaaaaa',
  labelColor: '#999999',
  labelSize: 13,
  currentStepLabelColor: '#fe7013',
};

const styleComponent = EnrolComponentStyle.getStyle();
class EnrolComponent extends React.Component {
  formEnrolement: EnrolModel;
  constructor(props) {
    super(props);
    this.formEnrolement = this.props.enrolementData;
    this.state = {
      load: false,
      refresh: true,
      confirm: false,
      validFormShow: false,
      modal: false,
      type: null,
      bourses: [],
      listes: [],
      libelle: '',
      datas: [],
      currentPosition: 0,
      prev: true,
      datasEnrol: [],
    };
    this.loadData();
  }
  refresh() {
    this.setState({refresh: true});
  }

  next() {
    if (this.state.currentPosition === 2) {
      this.savaData();
    } else {
      this.setState({
        currentPosition: this.state.currentPosition + 1,
        prev: false,
      });
    }
  }

  loadData() {
    SInfo.getItem('dataEnrol', {}).then(value => {
      if (value != null) {
        this.state.datasEnrol = JSON.parse(value);
        this.refresh();
      }
    });
  }

  prev() {
    if (this.state.currentPosition === 0) {
      this.setState({
        prev: true,
      });
    } else {
      this.setState({
        currentPosition: this.state.currentPosition - 1,
        prev: false,
      });
    }
  }
  componentDidMount() {
    // SInfo.deleteItem("dataEnrol", {});
    this.prev();
  }

  savaData() {
    this.state.datasEnrol.push(this.props.enrolementData);
    SInfo.setItem('dataEnrol', JSON.stringify(this.state.datasEnrol), {});
    this.setState({
      currentPosition: 3,
    });
  }

  render() {
    return (
      <Container>
        <Spinner visible={this.state.load} textContent={''} />
        <Header
          style={{
            backgroundColor: '#1a237e',
            height: 70,
          }}>
          <StatusBar backgroundColor="#000051" barStyle="light-content" />
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon style={styleComponent.color2} name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Text
              style={{
                color: 'white',
                fontSize: 18,
                right: 50,
                fontWeight: 'bold',
              }}>
              Fiche Enrôlement Individu
            </Text>
          </Body>
        </Header>
        <Content>
          <View style={{marginTop: 20, padding: 20}}>
            <StepIndicator
              customStyles={customStyles}
              currentPosition={this.state.currentPosition}
              labels={labels}
              stepCount={3}
            />
            {this.state.currentPosition === 0 && <FormEnrolComponent />}
            {this.state.currentPosition === 1 && <EmprunteEnrol />}
            {this.state.currentPosition === 2 && <ImageEnrol />}
            {this.state.currentPosition === 3 && (
              <View>
                <View style={styleComponent.justifyContent}>
                  <Image
                    style={{width: 140, height: 140}}
                    source={require('../../images/Validation_Icon.png')}
                  />
                  <Text style={styleComponent.textConfirmation}>
                    Opération effectuée avec succès
                  </Text>
                </View>
              </View>
            )}
            {this.state.currentPosition !== 3 && (
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <Button
                  iconLeft
                  rounded
                  disabled={this.state.prev}
                  style={{
                    width: 120,
                    marginTop: 50,
                    backgroundColor: '#004d40',
                    margin: 5,
                    opacity: this.state.prev ? 0.6 : 1,
                  }}
                  onPress={() => this.prev()}>
                  <Icon name="arrow-back" />
                  <Text style={{left: -5, fontSize: 12}}>Precedent</Text>
                </Button>
                <Button
                  iconRight
                  rounded
                  style={{
                    width: 120,
                    marginTop: 50,
                    backgroundColor: '#1a237e',
                  }}
                  onPress={() => this.next()}>
                  {this.state.currentPosition !== 2 && (
                    <Text style={{fontSize: 12}}>Suivant</Text>
                  )}
                  {this.state.currentPosition === 2 && (
                    <Text style={{fontSize: 12}}>Terminer</Text>
                  )}
                  <Icon name="arrow-forward" />
                </Button>
              </View>
            )}
            {this.state.currentPosition === 3 && (
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <Button
                  iconLeft
                  rounded
                  disabled={this.state.prev}
                  style={{
                    width: 120,
                    marginTop: 50,
                    backgroundColor: '#004d40',
                    margin: 5,
                  }}
                  onPress={() => this.props.navigation.goBack()}>
                  <Icon name="arrow-back" />
                  <Text style={{left: -5, fontSize: 12}}>Retour</Text>
                </Button>
              </View>
            )}
          </View>
        </Content>
      </Container>
    );
  }
}
const mapStateToProps = state => {
  return {enrolementData: state.enrolementData};
};
export default connect(mapStateToProps)(EnrolComponent);
