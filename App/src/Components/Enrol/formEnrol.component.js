import React from 'react';
import {
  View,
  Form,
  Item,
  Label,
  Input,
  Picker,
  Icon,
  DatePicker,
} from 'native-base';
import {EnrolComponentStyle} from './enrol.style';
import {connect} from 'react-redux';
import {EnrolModel} from '../../Shared/Models/enrol.model';
import {ENROL_MODEL} from '../../Shared/Utils/InputEnum';

const styleComponent = EnrolComponentStyle.getStyle();
class FormEnrolComponent extends React.Component {
  formEnrolement: EnrolModel;

  constructor(props) {
    super(props);
    this.formEnrolement = this.props.enrolementData;
    this.state = {
      load: false,
      currentPosition: 0,
      chosenDate: new Date(),
    };
    this.setDate = this.setDate.bind(this);
  }
  refresh() {
    this.setState({refresh: true});
  }
  componentDidMount() {}

  onValueChange2(value: string) {
    this.formEnrolement.natureDemande = value;
    this.refresh();
  }

  setSexVal(value: string) {
    this.formEnrolement.sexe = value;
    this.refresh();
  }

  setGroupeVal(value: string) {
    this.formEnrolement.groupeSanguin = value;
    this.refresh();
  }

  setDate(newDate) {
    this.formEnrolement.dateDemande = newDate;
    this.refresh();
  }

  maximumDate(): Date {
    let date = new Date();
    return date.setDate(date.getDate() + 1);
  }

  componentWillUnmount() {
    this.saveData();
  }

  saveData() {
    const action = {type: ENROL_MODEL.SET_INFOS, value: this.formEnrolement};
    this.props.dispatch(action);
  }

  render() {
    return (
      <View>
        <Form style={styleComponent.marginTop20}>
          <Item picker regular style={styleComponent.itemInput}>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{width: undefined}}
              placeholder="Nature de la demande"
              placeholderStyle={{color: '#bfc6ea'}}
              placeholderIconColor="#007aff"
              selectedValue={this.formEnrolement.natureDemande}
              onValueChange={this.onValueChange2.bind(this)}>
              <Picker.Item label="1ere Demande" value="premiere_demande" />
              <Picker.Item label="Renouvèlement" value="renouveelement" />
              <Picker.Item label="Duplicata" value="duplicata" />
            </Picker>
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.numeroRegistre}
              onChangeText={text => {
                this.formEnrolement.numeroRegistre = text;
                this.refresh();
              }}
              placeholder="Numero de registre"
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <DatePicker
              defaultDate={new Date()}
              maximumDate={new Date()}
              locale={'fr'}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={'fade'}
              androidMode={'default'}
              placeHolderText="Date de demande"
              textStyle={styleComponent.placeholderTextColor.color}
              placeHolderTextStyle={styleComponent.placeholderTextColor.color}
              onDateChange={this.setDate}
              disabled={false}
              style={{width: undefined}}
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.nom}
              onChangeText={text => {
                this.formEnrolement.nom = text;
                this.refresh();
              }}
              placeholder="Nom"
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.prenom}
              onChangeText={text => {
                this.formEnrolement.prenom = text;
                this.refresh();
              }}
              placeholder="Prénom"
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.profession}
              onChangeText={text => {
                this.formEnrolement.profession = text;
                this.refresh();
              }}
              placeholder="Profession"
            />
          </Item>
          <Item regular style={styleComponent.itemInput}>
            <DatePicker
              defaultDate={new Date()}
              maximumDate={new Date()}
              locale={'fr'}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={'fade'}
              androidMode={'default'}
              placeHolderText="Date de naissance "
              textStyle={styleComponent.placeholderTextColor.color}
              placeHolderTextStyle={styleComponent.placeholderTextColor.color}
              onDateChange={this.setDate}
              disabled={false}
              style={{width: undefined}}
            />
          </Item>
          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.localiteOrigine}
              onChangeText={text => {
                this.formEnrolement.localiteOrigine = text;
                this.refresh();
              }}
              placeholder="Localite d’origine au burkina "
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.nomPrenomPere}
              onChangeText={text => {
                this.formEnrolement.nomPrenomPere = text;
                this.refresh();
              }}
              placeholder="Nom et prenom du pere"
            />
          </Item>
          <Item regular style={styleComponent.itemInput}>
            <DatePicker
              defaultDate={new Date()}
              maximumDate={new Date()}
              locale={'fr'}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={'fade'}
              androidMode={'default'}
              placeHolderText="Date naissance pere "
              textStyle={styleComponent.placeholderTextColor.color}
              placeHolderTextStyle={styleComponent.placeholderTextColor.color}
              onDateChange={this.setDate}
              disabled={false}
              style={{width: undefined}}
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.nomPrenomMere}
              onChangeText={text => {
                this.formEnrolement.nomPrenomMere = text;
                this.refresh();
              }}
              placeholder="Nom et prenom de la mere "
            />
          </Item>
          <Item regular style={styleComponent.itemInput}>
            <DatePicker
              defaultDate={new Date()}
              maximumDate={new Date()}
              locale={'fr'}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={'fade'}
              androidMode={'default'}
              placeHolderText="Date naissance de la mere "
              textStyle={styleComponent.placeholderTextColor.color}
              placeHolderTextStyle={styleComponent.placeholderTextColor.color}
              onDateChange={this.setDate}
              disabled={false}
              style={{width: undefined}}
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.lieuResidence}
              onChangeText={text => {
                this.formEnrolement.lieuResidence = text;
                this.refresh();
              }}
              placeholder="Lieu de residence"
            />
          </Item>
          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.adresse}
              onChangeText={text => {
                this.formEnrolement.adresse = text;
                this.refresh();
              }}
              placeholder="Adresse"
            />
          </Item>
          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.telephone}
              onChangeText={text => {
                this.formEnrolement.telephone = text;
                this.refresh();
              }}
              placeholder="N°Telephone "
            />
          </Item>
          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.signeParticulier}
              onChangeText={text => {
                this.formEnrolement.signeParticulier = text;
                this.refresh();
              }}
              placeholder="Signes particuliers "
            />
          </Item>
          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.taille}
              onChangeText={text => {
                this.formEnrolement.taille = text;
                this.refresh();
              }}
              placeholder="Taille en cm "
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.teint}
              onChangeText={text => {
                this.formEnrolement.teint = text;
                this.refresh();
              }}
              placeholder="Teint"
            />
          </Item>
          <Item picker regular style={styleComponent.itemInput}>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{width: undefined}}
              placeholder="Groupe sanguin"
              placeholderStyle={{color: '#bfc6ea'}}
              placeholderIconColor="#007aff"
              selectedValue={this.formEnrolement.sexe}
              onValueChange={this.setGroupeVal.bind(this)}>
              <Picker.Item label="A+" value="A+" />
              <Picker.Item label="A-" value="A-" />
              <Picker.Item label="B+" value="B+" />
              <Picker.Item label="B-" value="B-" />
              <Picker.Item label="O+" value="O+" />
              <Picker.Item label="O-" value="O-" />
              <Picker.Item label="AB+" value="AB+" />
              <Picker.Item label="AB-" value="AB-" />
            </Picker>
          </Item>
          <Item picker regular style={styleComponent.itemInput}>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{width: undefined}}
              placeholder="Sexe"
              placeholderStyle={{color: '#bfc6ea'}}
              placeholderIconColor="#007aff"
              selectedValue={this.formEnrolement.sexe}
              onValueChange={this.setSexVal.bind(this)}>
              <Picker.Item label="Homme" value="Homme" />
              <Picker.Item label="Femme" value="Femme" />
            </Picker>
          </Item>
          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.personneContact}
              onChangeText={text => {
                this.formEnrolement.personneContact = text;
                this.refresh();
              }}
              placeholder="personne a contacter en cas de besoin (nom et prenom) "
            />
          </Item>
          <Item picker regular style={styleComponent.itemInput}>
            <Picker
              mode="dropdown"
              iosIcon={<Icon name="arrow-down" />}
              style={{width: undefined}}
              placeholder="Type de piece"
              placeholderStyle={{color: '#bfc6ea'}}
              placeholderIconColor="#007aff"
              selectedValue={this.formEnrolement.typeDocument}
              onValueChange={this.setGroupeVal.bind(this)}>
              <Picker.Item label="CNIB" value="CNIB" />
              <Picker.Item label="PASSPORT" value="PASSPORT" />
              <Picker.Item
                label="EXTRAIT DE NAISSANCE"
                value="EXTRAIT_DE_NAISSANCE"
              />
              <Picker.Item label="JUG SUPPLETIF" value="JUG_SUPPLETIF" />
              <Picker.Item label="ACTE DE MARIAGE" value="ACTE_DE_MARIAGE" />
              <Picker.Item label="AUTRE" value="AUTRE" />
            </Picker>
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.numeroPieceFournie}
              onChangeText={text => {
                this.formEnrolement.numeroPieceFournie = text;
                this.refresh();
              }}
              placeholder="Numero de la piece fournie "
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.nomAutoritePieceFournie}
              onChangeText={text => {
                this.formEnrolement.nomAutoritePieceFournie = text;
                this.refresh();
              }}
              placeholder="Nom autorite ayant etablit la piece "
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.codeDelegue}
              onChangeText={text => {
                this.formEnrolement.codeDelegue = text;
                this.refresh();
              }}
              placeholder="Code de delegue "
            />
          </Item>

          <Item regular style={styleComponent.itemInput}>
            <Input
              placeholderTextColor={styleComponent.placeholderTextColor.color}
              value={this.formEnrolement.nomDelegue}
              onChangeText={text => {
                this.formEnrolement.nomDelegue = text;
                this.refresh();
              }}
              placeholder="Nom delegue "
            />
          </Item>
        </Form>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {enrolementData: state.enrolementData};
};
export default connect(mapStateToProps)(FormEnrolComponent);
