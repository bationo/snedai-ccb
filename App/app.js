import React from 'react';
import {Provider} from 'react-redux';
import Store from '../App/src/Store/configureStore';
import AppContainer from './src/navigation';
import {Alert, BackHandler, StatusBar, View} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import Spinner from 'react-native-loading-spinner-overlay';

type Props = {};
export default class AppComponent extends React.Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      load: false,
    };
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentWillMount() {
    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }
  componentWillUnmount() {
    BackHandler.removeEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
  }

  handleBackButtonClick() {
    Alert.alert(
      'Avertissement',
      "Voulez-vous vraiment quitter l'application ",
      [
        {
          text: 'Annuler',
          style: 'cancel',
        },
        {
          text: 'Continuer',
          onPress: () => BackHandler.exitApp(),
        },
      ],
      {
        cancelable: false,
      },
    );

    return true;
  }

  componentDidMount() {
    /* setTimeout(() => {
      this.setState({ load: false });
      SplashScreen.hide();
    }, 2000);*/
  }

  render() {
    if (this.state.load) {
      return (
        <Spinner
          visible={this.state.load}
          textContent={''}
          textStyle={{color: '#ffffff'}}
        />
      );
    } else {
      return (
        <Provider store={Store}>
          <AppContainer />
        </Provider>
      );
    }
  }
}
